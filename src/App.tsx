import React from 'react';
import logo from './logo.svg';
import './App.css';
import AuthForm from './Components/AuthForm/AuthForm';

function App() {
  return (
    <div className="App">

      <header className="App-header">
        <h1> Домашнее задание по теме "Введение в React" </h1>
        <AuthForm />

      </header>

    </div>
  );
}

export default App;
