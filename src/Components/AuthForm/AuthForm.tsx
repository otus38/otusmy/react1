import { Component, PureComponent, ReactNode } from "react";


interface ComponentState {
    name: string;
    password: string;
    show: boolean;
}
export default class AuthForm extends Component<{}, ComponentState> {

    constructor(props: {}) {
        super(props);
        this.state = { name: 'user', password: 'password', show: true }
    }

    setPassword = (e: any) => {
        this.setState({ password: e.target.value });
    }

    setUsername = (e: any) => {
        this.setState({ name: e.target.value });
    }
    post = () => {
        const body = { username: this.state.name, password: this.state.password };
        fetch('/users', {
            method: 'POST',
            body: JSON.stringify(body)
        },)
            .then(x => console.log(x))
    }

    render() {
        return <>
            <label>Имя пользователя: </label><input type="text" onChange={this.setUsername} value={this.state.name} />
            <br />
            <label>Пароль: </label><input type="text" onChange={this.setPassword} value={this.state.password} />
            <br />
            <button onClick={this.post}>Отправить</button>
        </>;
    }
}